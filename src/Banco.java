import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class banco {

    public static void main(String[]args){

        CuentaCorriente cta1 = new CuentaCorriente("Javier", 1000, 258741);
        CuentaCorriente cta2 = new CuentaCorriente("Juana", 2000, 951159);

        Set<CuentaCorriente> listadoDeCeuntas = new HashSet<>();
        listadoDeCeuntas.add(cta1);
        listadoDeCeuntas.add(cta2);

        Operaciones.Transferir(cta1, cta2, 500);

        System.out.println("Salida: "+listadoDeCeuntas.toString());

        try {
            ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
                    "C:\Users\\PC\IdeaProjects\\30__Banco\\src\listadoDeCeuntas.dat"));

            flujoDeSalida.writeObject(listadoDeCeuntas);
            flujoDeSalida.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
                    "C:\Users\\PC\IdeaProjects\\30__Banco\\src\listadoDeCeuntas.dat"));

            Set<CuentaCorriente> listadoDeCeuntasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();

            System.out.println("Entrada: "+ listadoDeCeuntasEntrada.toString());

            flujoDeEntrada.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

class CuentaCorriente implements Serializable{

    private String nombreTitular;
    private double saldo;
    private int nroCuenta;

    public CuentaCorriente(String nombreTitular, double saldo, int nroCuenta) {
        this.nombreTitular = nombreTitular;
        this.saldo = saldo;
        this.nroCuenta = nroCuenta;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }

    public double verSaldo() {
        return saldo;
    }

    public void igresarDinero(double dinero) {
        this.saldo = this.saldo + dinero;
    }

    public void sacarDinero(double dinero) {
        this.saldo = this.saldo - dinero;
    }

    public int getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(int nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "nombreTitular='" + nombreTitular + '\'' +
                ", saldo=" + saldo +
                ", nroCuenta=" + nroCuenta +
                '}';
    }

}


class Operaciones{

    public static void Transferir(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto){
        cuentaEgreso.sacarDinero(monto);
        cuentaIngreso.igresarDinero(monto);
    }

}
